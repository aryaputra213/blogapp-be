import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';

const config: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'ec2-52-207-74-100.compute-1.amazonaws.com',
  port: 5432,
  username: 'wleevkurxnkbir',
  database: 'dbtta2tbd9vcg',
  password: 'fdd8ce43219c8f462d3f3e84725f7df8a8756fa716d1c40c2490f6ef821679df',
  entities: [__dirname + '/src/**/*.entity.{js,ts}'],
  synchronize: false,
  migrations: [join(__dirname, '/src/migrations/*{.ts,.js}')],
  cli: {
    migrationsDir: `src/migrations`,
  },
  ssl: {
    rejectUnauthorized: false,
  },
};

export default config;
