"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initDb1647011258522 = void 0;
class initDb1647011258522 {
    constructor() {
        this.name = 'initDb1647011258522';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "komentar" ("id" BIGSERIAL NOT NULL, "komentar_text" character varying(500) NOT NULL, "user_id" bigint, "blog_id" bigint, CONSTRAINT "PK_ce46d94a01d70df4392b4742a18" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("idUser" BIGSERIAL NOT NULL, "nama_lengkap" character varying(100) NOT NULL, "username" character varying(20) NOT NULL, "role" character varying NOT NULL, "password" character varying(255) NOT NULL, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "PK_c815460ecf7189b12a7ddd2d635" PRIMARY KEY ("idUser"))`);
        await queryRunner.query(`CREATE TABLE "blog" ("idBlog" BIGSERIAL NOT NULL, "blog_title" character varying(250) NOT NULL, "blog_content" character varying NOT NULL, "user_id" bigint, CONSTRAINT "PK_4a6a3b94a789190d8576afebde2" PRIMARY KEY ("idBlog"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "blog"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "komentar"`);
    }
}
exports.initDb1647011258522 = initDb1647011258522;
//# sourceMappingURL=1647011258522-init-db.js.map