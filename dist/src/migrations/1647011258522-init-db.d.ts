import { MigrationInterface, QueryRunner } from "typeorm";
export declare class initDb1647011258522 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
