import { User } from 'src/user/user.entity';
import { AuthService } from './auth.service';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    login(dataUser: User): Promise<{
        access_token: string;
        id: number;
        role: string;
        namaLengkap: string;
        username: string;
    }>;
}
