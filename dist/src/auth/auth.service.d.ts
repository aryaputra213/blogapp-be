import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/user.entity';
import { UserRepository } from 'src/user/user.repository';
export declare class AuthService {
    private readonly userRepository;
    private jwtService;
    constructor(userRepository: UserRepository, jwtService: JwtService);
    login(user: User): Promise<{
        access_token: string;
        id: number;
        role: string;
        namaLengkap: string;
        username: string;
    }>;
}
