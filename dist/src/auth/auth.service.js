"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const user_repository_1 = require("../user/user.repository");
const bcrypt = require("bcrypt");
let AuthService = class AuthService {
    constructor(userRepository, jwtService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
    }
    async login(user) {
        const dataUser = await this.userRepository.findOne({
            where: {
                username: user.username,
            },
        });
        if (dataUser) {
            const isMatch = await bcrypt.compare(user.password, dataUser.password);
            const payload = {
                namaLengkap: dataUser.namaLengkap,
                username: dataUser.username,
                role: dataUser.role,
            };
            if (isMatch) {
                return {
                    access_token: this.jwtService.sign(payload),
                    id: dataUser.idUser,
                    role: dataUser.role,
                    namaLengkap: dataUser.namaLengkap,
                    username: dataUser.username,
                };
            }
            throw new common_1.HttpException('wrong password', common_1.HttpStatus.UNAUTHORIZED);
        }
        throw new common_1.HttpException('wrong username ', common_1.HttpStatus.UNAUTHORIZED);
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map