import { UserRepository } from './user.repository';
import { User } from './user.entity';
export declare class UserService {
    private readonly userRepository;
    constructor(userRepository: UserRepository);
    createUser(dataUser: User): Promise<void>;
    getUser(idUser: number): Promise<User>;
    updateUser(idUser: number, dataUser: User): Promise<User>;
}
