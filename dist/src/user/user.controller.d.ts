import { User } from './user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    getUser(idUser: number): Promise<User>;
    registerUser(dataUser: User): Promise<void>;
    updateUser(idUser: number, dataUser: User): Promise<User>;
}
