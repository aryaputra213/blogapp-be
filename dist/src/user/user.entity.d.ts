import { Blog } from '../blog/blog.entity';
import { BaseEntity } from 'typeorm';
import { Komentar } from '../komentar/komentar.entity';
export declare class User extends BaseEntity {
    idUser: number;
    namaLengkap: string;
    username: string;
    role: string;
    password: string;
    blogs: Blog[];
    komentars: Komentar[];
}
