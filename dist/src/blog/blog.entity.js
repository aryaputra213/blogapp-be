"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Blog = void 0;
const user_entity_1 = require("../user/user.entity");
const typeorm_1 = require("typeorm");
const komentar_entity_1 = require("../komentar/komentar.entity");
let Blog = class Blog extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', { type: 'bigint' }),
    __metadata("design:type", Number)
], Blog.prototype, "idBlog", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'blog_title',
        type: 'varchar',
        length: 250,
        nullable: false,
    }),
    __metadata("design:type", String)
], Blog.prototype, "blogTitle", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'blog_content',
        type: 'varchar',
        nullable: false,
    }),
    __metadata("design:type", String)
], Blog.prototype, "blogContent", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, (user) => user.blogs, {
        createForeignKeyConstraints: false,
    }),
    (0, typeorm_1.JoinColumn)({ name: 'user_id' }),
    __metadata("design:type", user_entity_1.User)
], Blog.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => komentar_entity_1.Komentar, (komentar) => komentar.blog, {
        createForeignKeyConstraints: false,
    }),
    __metadata("design:type", Array)
], Blog.prototype, "komentars", void 0);
Blog = __decorate([
    (0, typeorm_1.Entity)()
], Blog);
exports.Blog = Blog;
//# sourceMappingURL=blog.entity.js.map