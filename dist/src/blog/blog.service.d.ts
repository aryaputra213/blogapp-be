import { UserRepository } from 'src/user/user.repository';
import { Blog } from './blog.entity';
import { BlogRepository } from './blog.repository';
export declare class BlogService {
    private readonly blogRepository;
    private readonly userRepository;
    constructor(blogRepository: BlogRepository, userRepository: UserRepository);
    getAllBlog(): Promise<Blog[]>;
    getBlogById(idBlog: number): Promise<Blog>;
    createMyBlog(idUser: number, dataBlog: Blog): Promise<Blog>;
    updateBlog(id: number, dataBlog: Blog): Promise<Blog>;
    deleteBlog(id: number): Promise<boolean>;
}
