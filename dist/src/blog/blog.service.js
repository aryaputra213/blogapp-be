"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_repository_1 = require("../user/user.repository");
const blog_entity_1 = require("./blog.entity");
const blog_repository_1 = require("./blog.repository");
let BlogService = class BlogService {
    constructor(blogRepository, userRepository) {
        this.blogRepository = blogRepository;
        this.userRepository = userRepository;
    }
    getAllBlog() {
        return this.blogRepository.find({
            relations: ['komentars'],
        });
    }
    getBlogById(idBlog) {
        return this.blogRepository.findOne(idBlog, {
            relations: ['komentars'],
        });
    }
    async createMyBlog(idUser, dataBlog) {
        const currentUser = await this.userRepository.findOne(idUser);
        if (currentUser) {
            const newBlog = new blog_entity_1.Blog();
            newBlog.blogTitle = dataBlog.blogTitle;
            newBlog.blogContent = dataBlog.blogContent;
            newBlog.user = currentUser;
            return newBlog.save();
        }
        throw new common_1.HttpException('No user with that ID', common_1.HttpStatus.NOT_FOUND);
    }
    async updateBlog(id, dataBlog) {
        const current = await this.blogRepository.findOne(id);
        if (current) {
            current.blogTitle = dataBlog.blogTitle
                ? dataBlog.blogTitle
                : current.blogTitle;
            current.blogContent = dataBlog.blogContent
                ? dataBlog.blogContent
                : current.blogContent;
            return current.save();
        }
    }
    async deleteBlog(id) {
        const current = await this.blogRepository.findOne(id);
        if (!current) {
            throw new common_1.HttpException(`Blog common with id ${id} not found`, common_1.HttpStatus.NOT_FOUND);
        }
        current.remove();
        return true;
    }
};
BlogService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(blog_repository_1.BlogRepository)),
    __metadata("design:paramtypes", [blog_repository_1.BlogRepository,
        user_repository_1.UserRepository])
], BlogService);
exports.BlogService = BlogService;
//# sourceMappingURL=blog.service.js.map