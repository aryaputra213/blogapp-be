import { Blog } from './blog.entity';
import { BlogService } from './blog.service';
export declare class BlogController {
    private readonly blogService;
    constructor(blogService: BlogService);
    getAllBlog(): Promise<Blog[]>;
    getBlogById(idBlog: number): Promise<Blog>;
    createBlog(idUser: number, dataBlog: Blog): Promise<Blog>;
    updateBlog(idBlog: number, dataBlog: Blog): Promise<Blog>;
    deleteBlog(idBlog: number): Promise<boolean>;
}
