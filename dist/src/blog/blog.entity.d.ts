import { User } from '../user/user.entity';
import { BaseEntity } from 'typeorm';
import { Komentar } from '../komentar/komentar.entity';
export declare class Blog extends BaseEntity {
    idBlog: number;
    blogTitle: string;
    blogContent: string;
    user: User;
    komentars: Komentar[];
}
