import { Repository } from 'typeorm';
import { Blog } from './blog.entity';
export declare class BlogRepository extends Repository<Blog> {
}
