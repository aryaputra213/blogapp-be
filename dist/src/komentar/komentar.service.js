"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KomentarService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const blog_repository_1 = require("../blog/blog.repository");
const user_repository_1 = require("../user/user.repository");
const komentar_entity_1 = require("./komentar.entity");
const komentar_repository_1 = require("./komentar.repository");
let KomentarService = class KomentarService {
    constructor(komentarRepository, userRepository, blogRepository) {
        this.komentarRepository = komentarRepository;
        this.userRepository = userRepository;
        this.blogRepository = blogRepository;
    }
    async getKomentarById(id) {
        return this.komentarRepository.findOne(id, {
            relations: ['user', 'blog'],
        });
    }
    async createKomentar(dataKomentar, idUser, idBlog) {
        const user = await this.userRepository.findOne(idUser);
        const blog = await this.blogRepository.findOne(idBlog);
        const newKomentar = new komentar_entity_1.Komentar();
        newKomentar.komentarText = dataKomentar.komentarText;
        newKomentar.user = user;
        newKomentar.blog = blog;
        return newKomentar.save();
    }
    async updateKomentar(dataKomentar, idKomentar) {
        const oldKomentar = await this.komentarRepository.findOne(idKomentar);
        if (oldKomentar) {
            oldKomentar.komentarText = dataKomentar.komentarText
                ? dataKomentar.komentarText
                : oldKomentar.komentarText;
            return oldKomentar.save();
        }
    }
    async deleteKomentar(idKomentar) {
        const current = await this.komentarRepository.findOne(idKomentar);
        if (!current) {
            throw new common_1.HttpException(`Komentar common with id ${idKomentar} not found`, common_1.HttpStatus.NOT_FOUND);
        }
        current.remove();
        return true;
    }
};
KomentarService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(komentar_repository_1.KomentarRepository)),
    __metadata("design:paramtypes", [komentar_repository_1.KomentarRepository,
        user_repository_1.UserRepository,
        blog_repository_1.BlogRepository])
], KomentarService);
exports.KomentarService = KomentarService;
//# sourceMappingURL=komentar.service.js.map