"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Komentar = void 0;
const blog_entity_1 = require("../blog/blog.entity");
const user_entity_1 = require("../user/user.entity");
const typeorm_1 = require("typeorm");
let Komentar = class Komentar extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', { type: 'bigint' }),
    __metadata("design:type", Number)
], Komentar.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'komentar_text',
        type: 'varchar',
        length: 500,
        nullable: false,
    }),
    __metadata("design:type", String)
], Komentar.prototype, "komentarText", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, (user) => user.komentars, {
        createForeignKeyConstraints: false,
    }),
    (0, typeorm_1.JoinColumn)({ name: 'user_id' }),
    __metadata("design:type", user_entity_1.User)
], Komentar.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => blog_entity_1.Blog, (blog) => blog.komentars, {
        createForeignKeyConstraints: false,
    }),
    (0, typeorm_1.JoinColumn)({ name: 'blog_id' }),
    __metadata("design:type", blog_entity_1.Blog)
], Komentar.prototype, "blog", void 0);
Komentar = __decorate([
    (0, typeorm_1.Entity)()
], Komentar);
exports.Komentar = Komentar;
//# sourceMappingURL=komentar.entity.js.map