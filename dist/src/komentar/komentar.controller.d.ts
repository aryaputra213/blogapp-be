import { Komentar } from './komentar.entity';
import { KomentarService } from './komentar.service';
export declare class KomentarController {
    private readonly komentarService;
    constructor(komentarService: KomentarService);
    getKomentarById(id: number): Promise<Komentar>;
    createKomentar(idUser: number, idBlog: number, dataKomentar: Komentar): Promise<Komentar>;
    updateKomentar(id: number, dataKomentar: Komentar): Promise<Komentar>;
    deleteKomentar(id: number): Promise<boolean>;
}
