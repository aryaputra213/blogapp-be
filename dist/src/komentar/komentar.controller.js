"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KomentarController = void 0;
const common_1 = require("@nestjs/common");
const jwt_auth_guards_1 = require("../auth/jwt-auth.guards");
const komentar_entity_1 = require("./komentar.entity");
const komentar_service_1 = require("./komentar.service");
let KomentarController = class KomentarController {
    constructor(komentarService) {
        this.komentarService = komentarService;
    }
    getKomentarById(id) {
        return this.komentarService.getKomentarById(id);
    }
    createKomentar(idUser, idBlog, dataKomentar) {
        return this.komentarService.createKomentar(dataKomentar, idUser, idBlog);
    }
    updateKomentar(id, dataKomentar) {
        return this.komentarService.updateKomentar(dataKomentar, id);
    }
    deleteKomentar(id) {
        return this.komentarService.deleteKomentar(id);
    }
};
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], KomentarController.prototype, "getKomentarById", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guards_1.JwtAuthGuard),
    (0, common_1.Post)('create/:idUser/:idBlog'),
    __param(0, (0, common_1.Param)('idUser')),
    __param(1, (0, common_1.Param)('idBlog')),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, komentar_entity_1.Komentar]),
    __metadata("design:returntype", void 0)
], KomentarController.prototype, "createKomentar", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guards_1.JwtAuthGuard),
    (0, common_1.Put)(':id'),
    __param(0, (0, common_1.Param)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, komentar_entity_1.Komentar]),
    __metadata("design:returntype", void 0)
], KomentarController.prototype, "updateKomentar", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guards_1.JwtAuthGuard),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], KomentarController.prototype, "deleteKomentar", null);
KomentarController = __decorate([
    (0, common_1.Controller)('komentar'),
    __metadata("design:paramtypes", [komentar_service_1.KomentarService])
], KomentarController);
exports.KomentarController = KomentarController;
//# sourceMappingURL=komentar.controller.js.map