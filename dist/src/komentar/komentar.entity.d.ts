import { Blog } from '../blog/blog.entity';
import { User } from '../user/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Komentar extends BaseEntity {
    id: number;
    komentarText: string;
    user: User;
    blog: Blog;
}
