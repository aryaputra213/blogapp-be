import { BlogRepository } from 'src/blog/blog.repository';
import { UserRepository } from 'src/user/user.repository';
import { Komentar } from './komentar.entity';
import { KomentarRepository } from './komentar.repository';
export declare class KomentarService {
    private readonly komentarRepository;
    private readonly userRepository;
    private readonly blogRepository;
    constructor(komentarRepository: KomentarRepository, userRepository: UserRepository, blogRepository: BlogRepository);
    getKomentarById(id: number): Promise<Komentar>;
    createKomentar(dataKomentar: Komentar, idUser: number, idBlog: number): Promise<Komentar>;
    updateKomentar(dataKomentar: Komentar, idKomentar: number): Promise<Komentar>;
    deleteKomentar(idKomentar: number): Promise<boolean>;
}
