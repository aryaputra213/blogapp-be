import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserRepository } from './user.repository';
import * as bcrypt from 'bcrypt';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  async createUser(dataUser: User) {
    const current = await this.userRepository.findOne({
      where: { username: dataUser.username },
    });

    if (current === undefined) {
      const saltOrRounds = 10;
      const hash = await bcrypt.hash(dataUser.password, saltOrRounds);
      const newUser = new User();
      newUser.namaLengkap = dataUser.namaLengkap;
      newUser.username = dataUser.username;
      newUser.password = hash;
      newUser.role = 'PEMBACA';
      newUser.save();
      throw new HttpException(newUser, HttpStatus.OK);
    }
    throw new HttpException('Username already used', HttpStatus.UNAUTHORIZED);
  }

  async getUser(idUser: number) {
    return this.userRepository.findOne(idUser, {
      relations: ['blogs', 'komentars'],
    });
  }

  async updateUser(idUser: number, dataUser: User) {
    const current = await this.userRepository.findOne(idUser);

    if (current) {
      current.namaLengkap = dataUser.namaLengkap
        ? dataUser.namaLengkap
        : current.namaLengkap;
      return current.save();
    }
  }
}
