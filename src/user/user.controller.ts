import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ExtractJwt } from 'passport-jwt';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guards';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UseGuards(JwtAuthGuard)
  @Get(':idUser')
  getUser(@Param() idUser: number) {
    return this.userService.getUser(idUser);
  }

  @Post()
  registerUser(@Body() dataUser: User) {
    return this.userService.createUser(dataUser);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':idUser')
  updateUser(@Param() idUser: number, @Body() dataUser: User) {
    return this.userService.updateUser(idUser, dataUser);
  }
}
