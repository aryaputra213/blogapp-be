import { Blog } from '../blog/blog.entity';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Komentar } from '../komentar/komentar.entity';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  idUser: number;

  @Column({
    name: 'nama_lengkap',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  namaLengkap: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 20,
    nullable: false,
    unique: true,
  })
  username: string;

  @Column({
    name: 'role',
    type: 'varchar',
    nullable: false,
  })
  role: string;

  @Column({ name: 'password', type: 'varchar', length: 255, nullable: false })
  password: string;

  @OneToMany(() => Blog, (blog) => blog.user, {
    createForeignKeyConstraints: false,
  })
  blogs: Blog[];

  @OneToMany(() => Komentar, (komentar) => komentar.user, {
    createForeignKeyConstraints: false,
  })
  komentars: Komentar[];
}
