import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guards';
import { Blog } from './blog.entity';
import { BlogService } from './blog.service';

@Controller('blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  @Get()
  getAllBlog() {
    return this.blogService.getAllBlog();
  }

  @Get(':idBlog')
  getBlogById(@Param() idBlog: number) {
    return this.blogService.getBlogById(idBlog);
  }

  @UseGuards(JwtAuthGuard)
  @Post(':idUser')
  createBlog(@Param() idUser: number, @Body() dataBlog: Blog) {
    return this.blogService.createMyBlog(idUser, dataBlog);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':idBlog')
  updateBlog(@Param() idBlog: number, @Body() dataBlog: Blog) {
    return this.blogService.updateBlog(idBlog, dataBlog);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':idBlog')
  deleteBlog(@Param() idBlog: number) {
    return this.blogService.deleteBlog(idBlog);
  }
}
