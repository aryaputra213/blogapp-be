import { User } from '../user/user.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Komentar } from '../komentar/komentar.entity';

@Entity()
export class Blog extends BaseEntity {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  idBlog: number;

  @Column({
    name: 'blog_title',
    type: 'varchar',
    length: 250,
    nullable: false,
  })
  blogTitle: string;

  @Column({
    name: 'blog_content',
    type: 'varchar',
    nullable: false,
  })
  blogContent: string;

  @ManyToOne(() => User, (user) => user.blogs, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToMany(() => Komentar, (komentar) => komentar.blog, {
    createForeignKeyConstraints: false,
  })
  komentars: Komentar[];
}
