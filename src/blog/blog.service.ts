import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { UserRepository } from 'src/user/user.repository';
import { Blog } from './blog.entity';
import { BlogRepository } from './blog.repository';

@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(BlogRepository)
    private readonly blogRepository: BlogRepository,
    private readonly userRepository: UserRepository,
  ) {}

  getAllBlog() {
    return this.blogRepository.find({
      relations: ['komentars'],
    });
  }

  getBlogById(idBlog: number) {
    return this.blogRepository.findOne(idBlog, {
      relations: ['komentars'],
    });
  }

  async createMyBlog(idUser: number, dataBlog: Blog) {
    const currentUser = await this.userRepository.findOne(idUser);

    if (currentUser) {
      const newBlog = new Blog();
      newBlog.blogTitle = dataBlog.blogTitle;
      newBlog.blogContent = dataBlog.blogContent;
      newBlog.user = currentUser;
      return newBlog.save();
    }
    throw new HttpException('No user with that ID', HttpStatus.NOT_FOUND);
  }

  async updateBlog(id: number, dataBlog: Blog) {
    const current = await this.blogRepository.findOne(id);

    if (current) {
      current.blogTitle = dataBlog.blogTitle
        ? dataBlog.blogTitle
        : current.blogTitle;

      current.blogContent = dataBlog.blogContent
        ? dataBlog.blogContent
        : current.blogContent;

      return current.save();
    }
  }

  async deleteBlog(id: number) {
    const current = await this.blogRepository.findOne(id);
    if (!current) {
      throw new HttpException(
        `Blog common with id ${id} not found`,
        HttpStatus.NOT_FOUND,
      );
    }
    current.remove();
    return true;
  }
}
