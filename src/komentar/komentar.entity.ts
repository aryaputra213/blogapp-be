import { Blog } from '../blog/blog.entity';
import { User } from '../user/user.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Komentar extends BaseEntity {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  id: number;

  @Column({
    name: 'komentar_text',
    type: 'varchar',
    length: 500,
    nullable: false,
  })
  komentarText: string;

  @ManyToOne(() => User, (user) => user.komentars, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ManyToOne(() => Blog, (blog) => blog.komentars, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'blog_id' })
  blog: Blog;
}
