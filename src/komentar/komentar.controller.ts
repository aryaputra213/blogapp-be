import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guards';
import { Komentar } from './komentar.entity';
import { KomentarService } from './komentar.service';

@Controller('komentar')
export class KomentarController {
  constructor(private readonly komentarService: KomentarService) {}

  @Get(':id')
  getKomentarById(@Param() id: number) {
    return this.komentarService.getKomentarById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('create/:idUser/:idBlog')
  createKomentar(
    @Param('idUser') idUser: number,
    @Param('idBlog') idBlog: number,
    @Body() dataKomentar: Komentar,
  ) {
    return this.komentarService.createKomentar(dataKomentar, idUser, idBlog);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  updateKomentar(@Param() id: number, @Body() dataKomentar: Komentar) {
    return this.komentarService.updateKomentar(dataKomentar, id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  deleteKomentar(@Param() id: number) {
    return this.komentarService.deleteKomentar(id);
  }
}
