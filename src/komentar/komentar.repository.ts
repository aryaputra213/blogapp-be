import { EntityRepository, Repository } from 'typeorm';
import { Komentar } from './komentar.entity';

@EntityRepository(Komentar)
export class KomentarRepository extends Repository<Komentar> {}
