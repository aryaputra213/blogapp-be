import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogRepository } from 'src/blog/blog.repository';
import { UserRepository } from 'src/user/user.repository';
import { KomentarController } from './komentar.controller';
import { KomentarRepository } from './komentar.repository';
import { KomentarService } from './komentar.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      KomentarRepository,
      UserRepository,
      BlogRepository,
    ]),
  ],
  controllers: [KomentarController],
  providers: [KomentarService],
})
export class KomentarModule {}
