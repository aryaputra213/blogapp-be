import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { BlogRepository } from 'src/blog/blog.repository';
import { UserRepository } from 'src/user/user.repository';
import { Komentar } from './komentar.entity';
import { KomentarRepository } from './komentar.repository';

@Injectable()
export class KomentarService {
  constructor(
    @InjectRepository(KomentarRepository)
    private readonly komentarRepository: KomentarRepository,
    private readonly userRepository: UserRepository,
    private readonly blogRepository: BlogRepository,
  ) {}

  async getKomentarById(id: number) {
    return this.komentarRepository.findOne(id, {
      relations: ['user', 'blog'],
    });
  }

  async createKomentar(dataKomentar: Komentar, idUser: number, idBlog: number) {
    const user = await this.userRepository.findOne(idUser);
    const blog = await this.blogRepository.findOne(idBlog);
    const newKomentar = new Komentar();
    newKomentar.komentarText = dataKomentar.komentarText;
    newKomentar.user = user;
    newKomentar.blog = blog;
    return newKomentar.save();
  }

  async updateKomentar(dataKomentar: Komentar, idKomentar: number) {
    const oldKomentar = await this.komentarRepository.findOne(idKomentar);
    if (oldKomentar) {
      oldKomentar.komentarText = dataKomentar.komentarText
        ? dataKomentar.komentarText
        : oldKomentar.komentarText;
      return oldKomentar.save();
    }
  }

  async deleteKomentar(idKomentar: number) {
    const current = await this.komentarRepository.findOne(idKomentar);

    if (!current) {
      throw new HttpException(
        `Komentar common with id ${idKomentar} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    current.remove();
    return true;
  }
}
