import {MigrationInterface, QueryRunner} from "typeorm";

export class initDb1647011258522 implements MigrationInterface {
    name = 'initDb1647011258522'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "komentar" ("id" BIGSERIAL NOT NULL, "komentar_text" character varying(500) NOT NULL, "user_id" bigint, "blog_id" bigint, CONSTRAINT "PK_ce46d94a01d70df4392b4742a18" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("idUser" BIGSERIAL NOT NULL, "nama_lengkap" character varying(100) NOT NULL, "username" character varying(20) NOT NULL, "role" character varying NOT NULL, "password" character varying(255) NOT NULL, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "PK_c815460ecf7189b12a7ddd2d635" PRIMARY KEY ("idUser"))`);
        await queryRunner.query(`CREATE TABLE "blog" ("idBlog" BIGSERIAL NOT NULL, "blog_title" character varying(250) NOT NULL, "blog_content" character varying NOT NULL, "user_id" bigint, CONSTRAINT "PK_4a6a3b94a789190d8576afebde2" PRIMARY KEY ("idBlog"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "blog"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "komentar"`);
    }

}
