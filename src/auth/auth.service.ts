import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/user.entity';
import { UserRepository } from 'src/user/user.repository';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async login(user: User) {
    const dataUser = await this.userRepository.findOne({
      where: {
        username: user.username,
      },
    });
    if (dataUser) {
      const isMatch = await bcrypt.compare(user.password, dataUser.password);

      const payload = {
        namaLengkap: dataUser.namaLengkap,
        username: dataUser.username,
        role: dataUser.role,
      };

      if (isMatch) {
        return {
          access_token: this.jwtService.sign(payload),
          id: dataUser.idUser,
          role: dataUser.role,
          namaLengkap: dataUser.namaLengkap,
          username: dataUser.username,
        };
      }
      throw new HttpException('wrong password', HttpStatus.UNAUTHORIZED);
    }
    throw new HttpException('wrong username ', HttpStatus.UNAUTHORIZED);
  }
}
