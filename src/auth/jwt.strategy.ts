import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'RISTEK2022',
    });
  }

  async validate(payload: any) {
    return {
      namaLengkap: payload.namaLengkap,
      username: payload.username,
      role: payload.role,
    };
  }
}
