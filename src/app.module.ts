import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import config from '../ormconfig';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { BlogModule } from './blog/blog.module';
import { KomentarModule } from './komentar/komentar.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [TypeOrmModule.forRoot(config), UserModule, BlogModule, KomentarModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
